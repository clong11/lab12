#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libsocket/libinetsocket.h>
#include <ctype.h>


FILE * connect_to_server();
void menu();
char get_choice();
void list_files(FILE *s);
void download(FILE *s);
void quit(FILE *s);

int main()
{
    
    
    
    FILE * s = connect_to_server();
    char filename[100], c; 
     c = fgetc(s); 
    
    int a =0; 
    for( a = 0; a < 13; a = a + 1 ){
      
        printf ("%c", c); 
        c = fgetc(s); 
   }
     printf("\n");
   
  
    //fclose(s); 
    
    
    while(1){
        
    // Menu
    menu();
    
    // Get choice
    char choice = get_choice();
    
    // Handle choice
    switch(choice)
    {
        case 'l':
        case 'L':
            list_files(s);
            break;
        
        case 'd':
        case 'D':
        printf("Download\n");
            download(s);
            break;
            
        case 'q':
        case 'Q':
        printf("Goodbye\n");
            quit(s);
            exit(0);
            break;
            
        default:
            printf("Choice must be d, l, or q\n");
    }
    }
}

/*
 * Connect to server. Returns a FILE pointer that the
 * rest of the program will use to send/receive data.
 */
FILE * connect_to_server()
{
    printf("connecting to server");
    
    int socknum = create_inet_stream_socket("runwire.com","1234",LIBSOCKET_IPv4,0);
    if(socknum == -1){
        fprintf(stderr,"Couldn't connect\n");   
    }
    
    
    // convert
    FILE *f= fdopen(socknum,"r+");
    if(!f){
        fprintf(stderr,"Could not convert socket num\n");
        exit(2);
    }
    
    return f;
}

/*
 * Display menu of choices.
 */
void menu()
{
    printf("[L]List files\n");
    printf("[D]Download a file\n");
    printf("[Q]Quit\n");
    printf("\nChoice: ");
}

/*
 * Get the menu choice from the user. Allows the user to
 * enter up to 100 characters, but only the first character
 * is returned.
 */
char get_choice()
{
  //  printf("Your choice: ");
    char buf[100];
    fgets(buf, 100, stdin);
    return buf[0];
}

/*
 * Display a file list to the user.
 */
void list_files(FILE *s)
{
    
        printf("List Files\n");
    //send list command to server
    fprintf(s, "LIST\n");
    
    
    //print out s with new dat in it
    
     printf("\n");
    int periodCheck = 0;
    char line[1000] = "";
    //skip over +OK line
    fgets(line, 1000, s);
    
    
    while(strncmp(&line[0], ".", 3) != 0)
    {
        if(strncmp(&line[0], "+OK", 3) != 0)
        {
            
            //checks if a line only contains a period and stops listing lines once it finds only a period
            for(int i = 0; line[i] != '\0'; ++i)
              {
                if(line[i] == '.')
                {
                  periodCheck = 1;
                  break;
                }else{
                    periodCheck = 0;
                    break;
                }
              }
            if(periodCheck == 1){
                //printf("found period by itself.. stop list");
                break;
            }
            
            fgets(line, 1000, s);
            printf("%s", line);
            
        }
        else 
        {
            fgets(line, 1000, s);
        }
    }
    
    
    // fgets(line, 1000, s);
    // printf("%s",line);
    // fgets(line, 1000, s);
    // printf("%s",line);
    // fgets(line, 1000, s);
    // printf("%s",line);
    // fgets(line, 1000, s);
    // printf("%s",line);
}

/*
 * Download a file.
 * Prompt the user to enter a filename.
 * Download it from the server and save it to a file with the
 * same name.
 */
void download(FILE *s)
{
    //get user input as a string
    printf("Enter filename of file you wish to download: ");
     char filename[20],str[20]; 
     char line2[1000]="";
    long unsigned int size = 0;
     //store input in str
    scanf("%[^\n]%*c", filename); 
     printf("Downloading %s...\n", filename); 
    
    //get size of the file
    
    fprintf(s,"SIZE %s\n",filename);
    fgets(line2,1000,s);
    printf("%s",line2);
    //fgets(line2,1000,s);
    
    if(line2[0] == '+'){
        line2[strlen(line2)] = '\0';
        sscanf(line2,"%s%lu",str,&size);
    printf("Size of file is %lu\n",size);
        
    }
    // char line2[1000]="";
    // sscanf(line2,"%s%lu",str,&size);
    // printf("Size of file is %lu\n",size);
    
    
     fprintf(s,"GET %s\n",filename);
    int doWrite = 1;
    FILE * destFile;
    //check if file exists
     if((destFile = fopen(filename,"r"))!=NULL)
        {
            // file exists
            char answer;
            printf("A file called %s already exists in this folder.. Overwrite existing file? [Y/N]:  ",filename);
            scanf("%c",&answer);
            
            printf("%d",answer);
            
            if(answer == 121 || answer == 89)
            {
                doWrite = 1;
                destFile = fopen (filename,"w");
            }
            else{
                doWrite  =0;
                
            }
            
            
            //fclose(filename);
        }
        else{
            doWrite = 1;
            destFile = fopen (filename,"w");
        }
    
    unsigned char data[100];
    int so_far = -4;
    int got = -4;
    int length = size;
    if(doWrite == 1){
    //get all the chunks of 100 bytes
    while(size-so_far >= 100){
        got = fread(data,sizeof(unsigned char),100,s);
    
        //fprintf(stderr,"Got: %d\n",got);
      //  fwrite(data,sizeof(unsigned char),got,stdout);
        fwrite(data,sizeof(unsigned char),got,destFile);
        so_far += got;
        //printf("\nso_far: %d   got: %d\n",so_far,got);
        //if(so_far == size) break;
        //progress bar
        printf("\033[22D[");
        for(int i = 0;i<20;i++){
            if(((double)so_far/(double)length)*100 < i*5){
                printf("-");
                
            }
            else{
                printf("=");
            }
        }
        printf("]");
        
    }
    
    //get the remaining chunk of <100 bytes
    if(size-so_far > 0){
        got = fread(data,sizeof(unsigned char),size-so_far,s);
        //fprintf(stderr,"Got: %d\n",got);
       // fwrite(data,sizeof(unsigned char),got,stdout);
        fwrite(data,sizeof(unsigned char),got,destFile);
        
    }
     fclose(destFile);
    printf("\nDownload finished!\n");
    }
    else{printf("\nDownload aborted!\n");}
    fflush(stdin);
    fflush(stdout);
}

/* 
 * Close the connection to the server.
 */
void quit(FILE *s)
{
    fclose(s);
}